#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hash_guess = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strncmp(hash, hash_guess, HASH_LEN) == 0) return 1;
    else return 0;
    // Free any malloc'd memory
    free(hash_guess);
}
int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else 
        return fileinfo.st_size;
}
// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.

char **read_dictionary(char *filename, int *size)
{   
    //get length of file
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Could not get length of file %s\n", filename);
        exit(1);
    }
    //allocate mem for file
    char *file_contents = malloc(len);
    
    //read file into file contents
    FILE *fp = fopen(filename, "r");
    if(!fp)
    {
        printf("Could not open %s for reading\n",filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    //replace \n with NULL \0
    //keep count
    int line_count = 0;
    for (int i = 0; i < len; i++)
    {
        if(file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    //Allocate array of pointers to each line
    char **lines = malloc(line_count * sizeof(char*));
    
    //fill in each entry with adress of coresponding line
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        
        //scan forward to find next line
        while(file_contents[c] != '\0') c++;
        c++;
    }
    
    // return adress
    *size = line_count;
    return lines;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
   
 
    int dlen;
    int hlen;
    // Read the dictionary file into an array of strings.
    char **hash_file = read_dictionary(argv[1], &hlen);
    char **dict = read_dictionary(argv[2], &dlen);
    printf("\n");
    for(int i = 0; i < hlen; i++)
    {
        //printf("in hash loop\n");
        for(int c = 0; c < dlen; c++)
        {
            //printf("in dict loop\n");
            if (tryguess(hash_file[i], dict[c]) == 1)
            {
                printf("%s\n%s\n\n", hash_file[i], dict[c]);
            }
        }
    }
    // Print the matching dictionary entry.
    // Need two nested loops.
}
